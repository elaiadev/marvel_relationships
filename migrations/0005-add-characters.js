
var mongodb = require('mongodb');

exports.up = function(db, next){
    var characters = db.collection('characters');
    characters.insert(
    [
      {
        "name":"Captain America",
        "real_name":"Steve Rogers",
        "d":"d",
        "slug":"captain-america",
        "r":"r",
        "image_path":"http://i.annihil.us/u/prod/marvel/i/mg/3/50/537ba56d31087",
        "image_extension":"jpg"
      },
      {
        "name":"Wasp",
        "real_name":"Janet van Dyne",
        "d":"d",
        "slug":"wasp",
        "r":"r",
        "image_path":"http://i.annihil.us/u/prod/marvel/i/mg/9/c0/5390dfd5ef165",
        "image_extension":"jpg"
      },
      {
        "name":"Black Widow",
        "real_name":"Natasha Romanov",
        "d":"d",
        "slug":"black-widow",
        "r":"r",
        "image_path":"http://i.annihil.us/u/prod/marvel/i/mg/f/30/50fecad1f395b",
        "image_extension":"jpg"
      },
      {
        "name":"Iron Woman",
        "real_name":"Natasha Stark",
        "d":"d",
        "slug":"iron-woman",
        "r":"r",
        "image_path":"none",
        "image_extension":"none"
      },
      {
        "name":"Storm",
        "real_name":"Ororo Munroe",
        "d":"d",
        "slug":"storm",
        "r":"r",
        "image_path":"http://i.annihil.us/u/prod/marvel/i/mg/6/40/526963dad214d",
        "image_extension":"jpg"
      },
      {
        "name":"Scarlet Witch",
        "real_name":"Wanda Maximoff",
        "d":"d",
        "slug":"scarlet-witch",
        "r":"r",
        "image_path":"http://i.annihil.us/u/prod/marvel/i/mg/6/70/5261a7d7c394b",
        "image_extension":"jpg"
      },
      {
        "name":"Mystique",
        "real_name":"Raven Darkholme",
        "d":"d",
        "slug":"mystique",
        "r":"r",
        "image_path":"http://i.annihil.us/u/prod/marvel/i/mg/5/03/5390dc2225782",
        "image_extension":"jpg"
      },
      {
        "name":"Rogue",
        "real_name":"Anna Marie",
        "d":"d",
        "slug":"rogue",
        "r":"r",
        "image_path":"http://i.annihil.us/u/prod/marvel/i/mg/3/10/5112d84e2166c",
        "image_extension":"jpg"
      },
      {
        "name":"Phoenix",
        "real_name":"Jean Gray",
        "d":"d",
        "slug":"phoenix",
        "r":"r",
        "image_path":"none",
        "image_extension":"none"
      },
      {
        "name":"Cyclops",
        "real_name":"Scott Summers",
        "d":"d",
        "slug":"cyclops",
        "r":"r",
        "image_path":"http://i.annihil.us/u/prod/marvel/i/mg/6/70/526547e2d90ad",
        "image_extension":"jpg"
      },
      {
        "name":"Havok",
        "real_name":"Alex Summers",
        "d":"d",
        "slug":"havok",
        "r":"r",
        "image_path":"http://i.annihil.us/u/prod/marvel/i/mg/9/e0/5261659ebeaf8",
        "image_extension":"jpg"
      },
      {
        "name":"Wolverine",
        "real_name":"James \"Logan\" Howlett",
        "d":"d",
        "slug":"wolverine",
        "r":"r",
        "image_path":"http://i.annihil.us/u/prod/marvel/i/mg/2/60/537bcaef0f6cf",
        "image_extension":"jpg"
      },
      {
        "name":"Gambit",
        "real_name":"Remy LeBeau",
        "d":"d",
        "slug":"gambit",
        "r":"r",
        "image_path":"http://i.annihil.us/u/prod/marvel/i/mg/a/40/52696aa8aee99",
        "image_extension":"jpg"
      },
      {
        "name":"Nightcrawler",
        "real_name":"Kurt Wagner",
        "d":"d",
        "slug":"nightcrawler",
        "r":"r",
        "image_path":"http://i.annihil.us/u/prod/marvel/i/mg/1/40/526034979bc98",
        "image_extension":"jpg"
      },
      {
        "name":"Magneto",
        "real_name":"Erik Lehnsherr",
        "d":"d",
        "slug":"magneto",
        "r":"r",
        "image_path":"http://i.annihil.us/u/prod/marvel/i/mg/3/b0/5261a7e53f827",
        "image_extension":"jpg"
      },
      {
        "name":"Quicksilver",
        "real_name":"Pietro Maximoff",
        "d":"d",
        "slug":"quicksilver",
        "r":"r",
        "image_path":"http://i.annihil.us/u/prod/marvel/i/mg/6/f0/53176ffc42f58",
        "image_extension":"jpg"
      },
      {
        "name":"Professor X",
        "real_name":"Charles Xavier",
        "d":"d",
        "slug":"professor-x",
        "r":"r",
        "image_path":"http://i.annihil.us/u/prod/marvel/i/mg/3/e0/528d3378de525",
        "image_extension":"jpg"
      },
      {
        "name":"Black Panther",
        "real_name":"T'challa",
        "d":"d",
        "slug":"black-panther",
        "r":"r",
        "image_path":"http://i.annihil.us/u/prod/marvel/i/mg/6/60/5261a80a67e7d",
        "image_extension":"jpg"
      },
      {
        "name":"Red Skull",
        "real_name":"Johann Shmidt",
        "d":"d",
        "slug":"red-skull",
        "r":"r",
        "image_path":"http://i.annihil.us/u/prod/marvel/i/mg/2/03/526036550cd37",
        "image_extension":"jpg"
      },
      {
        "name":"Winter Soldier",
        "real_name":"James Buchanan Barnes",
        "d":"d",
        "slug":"winter-soldier",
        "r":"r",
        "image_path":"http://i.annihil.us/u/prod/marvel/i/mg/d/03/5265478293c1e",
        "image_extension":"jpg"
      },
      {
        "name":"Iron Man",
        "real_name":"Tony Stark",
        "d":"d",
        "slug":"iron-man",
        "r":"r",
        "image_path":"http://i.annihil.us/u/prod/marvel/i/mg/9/c0/527bb7b37ff55",
        "image_extension":"jpg"
      },
      {
        "name":"Hawkeye",
        "real_name":"Clint Barton",
        "d":"d",
        "slug":"hawkeye",
        "r":"r",
        "image_path":"http://i.annihil.us/u/prod/marvel/i/mg/e/90/50fecaf4f101b",
        "image_extension":"jpg"
      }
    ]  
      , function(err)
    {
        if(err){return next(err);}
	next();
    });
};

exports.down = function(db, next){
    var characters = db.collection('characters');
    characters.remove({}, function(err)
    {
	if(err){return next(err);}
        next();
    });
};
