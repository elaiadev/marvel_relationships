// app declaration
var app = angular.module('comicconnections', ['ui.router']);

// states
app.config(
[
	'$stateProvider',
	'$urlRouterProvider',
	function($stateProvider, $urlRouterProvider)
	{
		$stateProvider
			.state(
				'splash',
				{
					url: '/splash',
					templateUrl: '/views/pages/splash.html'
				}
			)

			.state(
				'login',
				{
					url: '/login',
					templateUrl: 'views/pages/login.html',
					controller: 'auth_controller',
					onEnter:
					[
						'$state',
						'auth',
						function($state, auth)
						{
							if(auth.is_logged_in())
								$state.go('characters');
						}
					]
				}
			)

			.state(
				'register',
				{
					url: '/register',
					templateUrl: 'views/pages/register.html',
					controller: 'auth_controller',
					onEnter:
					[
						'$state',
						'auth',
						function($state, auth)
						{
							if(auth.is_logged_in())
								$state.go('characters');
						}
					]
				}
			)

			.state(
				'characters',
				{
					url: '/characters',
					templateUrl: 'views/pages/characters.html',
					controller: 'character_controller',
					resolve:
					{
						promise:
						[
							'characters',
							function(characters)
							{
								return characters.get_all();
							}
						]
					}
				}
			)

			.state(
				'relationships',
				{
					url: '/relationships/{slug}',
					templateUrl: 'views/pages/relationships.html',
					controller: 'relationship_controller',
					resolve:
					{
						character_promise:
						[
							'$stateParams',
							'characters',
							function($stateParams, characters)
							{
								characters.get_single($stateParams.slug);
								return characters.get_all();
							}
						],
						relationship_promise:
						[
							'$stateParams',
							'relationships',
							function($stateParams, relationships)
							{
								return relationships.get_all($stateParams.slug);
							}
						]
					}
				}
			)

			.state(
				'comparison',
				{
					url: '/comparison?name1&name2',
					templateUrl: 'views/pages/comparison.html',
					controller: 'comparison_controller',
					resolve:
					{
						all_character_promise:
						[
							'$stateParams',
							'characters',
							function($stateParams, characters)
							{
								return characters.get_all();
							}
						],
						comparison_characters_promise:
						[
							'$stateParams',
							'characters',
							function($stateParams, characters)
							{
								return characters.get_two($stateParams.name1,$stateParams.name2);
							}
						],
						relationship_promise:
						[
							'$stateParams',
							'relationships',
							function($stateParams, relationships)
							{
								return relationships.get_common($stateParams.name1,$stateParams.name2);
							}
						]
					}
				}
			)
			;
		$urlRouterProvider.otherwise('splash');
	}
]);

// services/factories
app.factory(
	'characters',
	[
		'$http',
		function($http)
		{
			// object
			var o = { characters: [], current_character: {}, compare_characters: [] };

			/* METHODS */
			o.get_all = function()
			{
				o.characters = [];
				return $http.get('/characters').success(function(data)
				{
					angular.copy(data, o.characters);
					o.characters.sort(function(a,b){return (a.name).localeCompare(b.name)});
				});
			};

			o.get_single = function(id)
			{
				o.current_character = {};
				return $http.get('/characters/' + id).success(function(data)
				{
					o.current_character = data;
				});
			};

			o.get_two = function(name1, name2)
			{
				o.compare_characters = [];
				return $http.get('/comparison/characters?char1=' + name1 + '&char2=' + name2).success(function(data)
					{
						if(data[1].name === name1)
						{
							o.compare_characters[0] = data[1];
							o.compare_characters[1] = data[0];
						}
						else
						{
							o.compare_characters[0] = data[0];
							o.compare_characters[1] = data[1];
						}
					});
			};

			return o;
		}
	]
);

app.factory(
	'relationships',
	[
		'$http',
		'characters',
		function($http, characters)
		{
			var o = { relationships: [] }

			o.get_common = function(name1, name2)
			{
				return $http.get('/comparison?name1=' + name1 + '&name2=' + name2).success(function(data)
					{
						o.relationships = data;
					});
			};

			o.get_all = function(slug)
			{
				o.relationships = [];
				return $http.get('/characters/' + slug).success(function(data)
				{
					name = data.name;
					return $http.get('/relationships/' + name).success(function(data)
					{
						var types = [];
						var single = 0;
						for(var i = 0; i < data.length; i++)
						{
							var members = [];
							var summary = [];
							var relationship = data[i];
							var type = relationship.type;
							if(types.indexOf(type) == -1)
							{
								types.push(type);
								for(var j = 0; j < data.length; j++)
								{
									if(data[j].type == type)
										{
											var nono = {};
											nono.character_name = data[j].name;
											nono.summary = data[j].summary;
											members.push(nono);
										}
								}
								o.relationships.push({'name': type, 'members': members});
							}
						}
					});
				});
			};

			return o;
		}
	]
);

app.factory(
	'auth',
	[
		'$http',
		'$window',
		function($http, $window)
		{
			var auth = {};

			/* METHODS */
			auth.save_token = function(token)
			{
				$window.localStorage['comicconnections-token'] = token;
			};
			auth.get_token = function()
			{
				return $window.localStorage['comicconnections-token'];
			};
			auth.is_logged_in = function()
			{
				var token = auth.get_token();
				if(token)
				{
					var payload = JSON.parse($window.atob(token.split('.')[1]));
					return payload.exp > Date.now()/1000;
				}
				else
					return false;
			};
			auth.current_user = function()
			{
				if(auth.is_logged_in())
				{
					var token = auth.get_token();
					var payload = JSON.parse($window.atob(token.split('.')[1]));
					return payload.username;
				}
			};
			auth.register = function(user)
			{
				return $http.post('/register', user).success(function(data)
				{
					auth.save_token(data.token);
				});
			};
			auth.login = function(user)
			{
				return $http.post('/login', user).success(function(data)
				{
					auth.save_token(data.token);		
				});
			};
			auth.logout = function()
			{
				$window.localStorage.removeItem('comicconnections-token');
			};

			return auth;
		}
	]
);

// controllers
app.controller('character_controller',
	[
		'$scope',
		'characters',
		'auth',
		function($scope, characters, auth)
		{
			$scope.isLogged = auth.is_logged_in;
			$scope.nana_name = auth.current_user;
			$scope.characters = characters.characters;
		}
	]
);

app.controller(
	'relationship_controller',
	[
		'$scope',
		'characters',
		'relationships',
		function($scope, characters, relationships)
		{
			$scope.characters = characters.characters;
			$scope.current_character = characters.current_character;
			$scope.relationships = relationships.relationships;
			$scope.start = 0;

			$scope.max_length = $scope.characters.length;

			$scope.match = function(character)
			{
				return character.name === $scope.current_character.name;
			};

			$scope.decrement_start = function()
			{
				console.log('# ' + $scope.start);
				if($scope.start <= 0)
					$scope.start = $scope.max_length - 1;
				else
					$scope.start -= 1;
				console.log('@ ' + $scope.start);
			};

			$scope.increment_start = function()
			{
				if($scope.start >= $scope.max_length - 1)
					$scope.start = 0;
				else
					$scope.start += 1;
			};

			$scope.calculate_current = function()
			{
				for(var i=0; i<$scope.characters.length; i++)
				{
					if($scope.match($scope.characters[i]))
						{
							$scope.start = i-6;
							if($scope.start < 0)
								$scope.start = $scope.max_length + (i - 6);
							break;
						}
				}
			};
			$scope.calculate_current();
		}
	]
);

app.controller(
	'comparison_controller',
	[
		'$scope',
		'characters',
		'relationships',
		function($scope, characters, relationships)
		{
			$scope.characters = characters.characters;
			$scope.relationships = relationships.relationships;
			$scope.top_character = characters.compare_characters[0];
			$scope.bottom_character = characters.compare_characters[1];

			$scope.start = 0;
			$scope.top_start = 0;
			$scope.bottom_start = 0;
			$scope.max_length = $scope.characters.length;
			$scope.max_number = 6;

			$scope.match = function(name1, name2)
			{
				return name1 === name2;
			};

			$scope.match_top = function(character)
			{
				return $scope.match(character.name, $scope.top_character.name);
			};

			$scope.match_bottom = function(character)
			{
				return $scope.match(character.name, $scope.bottom_character.name);
			};

			$scope.decrement_start = function(start)
			{
				if(start <= 0)
					start = $scope.max_length - 1;
				else
					start -= 1;
				return start;
			};

			$scope.increment_start = function(start)
			{
				if(start >= $scope.max_length - 1)
					start = 0;
				else
					start += 1;
				return start;
			};

			$scope.decrement_top = function()
			{
				$scope.top_start = $scope.decrement_start($scope.top_start);
			};

			$scope.increment_top = function()
			{
				$scope.top_start = $scope.increment_start($scope.top_start);
			};

			$scope.decrement_bottom = function()
			{
				$scope.bottom_start = $scope.decrement_start($scope.bottom_start);
			};

			$scope.increment_bottom = function()
			{
				$scope.bottom_start = $scope.increment_start($scope.bottom_start);
			};

			$scope.calculate_current = function(start)
			{
				for(var i=0; i < $scope.max_length; i++)
				{
					var current_name = $scope.characters[i].name;
					if($scope.match(current_name, $scope.top_character.name) || $scope.match(current_name, $scope.top_character.name))
					{
						start = i-$scope.max_number;
						if(start < 0)
							start = $scope.max_length + (i - $scope.max_number);
						break;
					}
				}
				return start;
			};

			$scope.calculate_current_top = function()
			{
				for(var i=0; i < $scope.max_length; i++)
				{
					var current_name = $scope.characters[i].name;
					if($scope.match(current_name, $scope.top_character.name) )
					{
						$scope.top_start = i-$scope.max_number;
						if($scope.top_start < 0)
							$scope.top_start = $scope.max_length + (i - $scope.max_number);
						break;
					}
				}
			};

			$scope.calculate_current_bottom = function()
			{
				for(var i=0; i < $scope.max_length; i++)
				{
					var current_name = $scope.characters[i].name;
					if($scope.match(current_name, $scope.bottom_character.name))
					{
						$scope.bottom_start = i-$scope.max_number;
						if($scope.bottom_start < 0)
							$scope.bottom_start = $scope.max_length + (i - $scope.max_number);
						break;
					}
				}
			};

			$scope.calculate_current_top();
			$scope.calculate_current_bottom();
		}
	]
);

app.controller('auth_controller',
	[
		'$scope',
		'$state',
		'auth',
		function($scope, $state, auth)
		{
			$scope.user = {};

			$scope.register = function()
			{
				console.log('we\'re here');
				auth.register($scope.user).error(function(error)
				{
					$scope.error = error;
				})
				.then(function()
				{
					$state.go('characters');
				});
			};

			$scope.login = function()
			{
				auth.login($scope.user).error(function(error)
				{
					$scope.error = error;
				})
				.then(function()
				{
					$state.go('characters');
				});
			};
		}
	]
);

app.controller('nav_controller',
	[
		'$scope',
		'auth',
		function($scope, auth)
		{
			$scope.is_logged_in = auth.is_logged_in;
			$scope.current_user = auth.current_user;
			$scope.logout = auth.logout;
		}
	]
);

/* Inspired by http://stackoverflow.com/questions/11581209/pagination-on-a-list-using-ng-repeat->http://jsfiddle.net/2ZzZB/56/ */
app.filter('startFrom', function()
{
	return function(input, start, characters) {
		start = +start; //parse to int
	
		var list_length = 13;

		if(start + list_length < characters.length)
			return input.slice(start);
		else
		{
			var arr = input.slice(start);
			var new_arr = characters.slice(0,start-Math.floor(list_length/2)); 
			return arr.concat(new_arr);
		}
	}
});