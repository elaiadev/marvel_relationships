var express = require('express');
var router = express.Router();
var jwt = require('express-jwt');
var passport = require('passport');
var mongoose = require('mongoose');

var Character = mongoose.model('Character');
var Relationship = mongoose.model('Relationship');
var User = mongoose.model('User');

var auth = jwt({secret: 'SECRET', userProperty: 'payload'});

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});

router.param('character', function(req, res, next, slug)
{
	// var query = Character.findById(id);
	Character.findOne({'slug': slug}, function(err, character)
	{
		if(err){return next(err);}
		if(!character){return next(new Error('can\'t find character'));}
		req.character = character;
		return next();
	});
});

router.param('relationship_single', function(req, res, next, name)
{
	var query = Relationship.find().or([{character_1: name}, {character_2: name}]);
	query.exec(function(err, relationships)
	{
		if(err){return next(err);}
		var packaged_relationships = package_relationships(name, relationships);
		packaged_relationships.sort(function(a,b)
		{
			return (a.name).localeCompare(b.name);
		});
		req.relationship_single = packaged_relationships;
		return next();
	});

});

var package_relationships = function(name, relationship)
{
	var all_relationships = [];
	var all_characters = [];
	for(var i=0; i < relationship.length; i++)
	{
		var current_rel = relationship[i];
		var new_rel = {};
		if(current_rel.character_1 === name)
			new_rel.name = current_rel.character_2;
		if(current_rel.character_2 === name)
			new_rel.name = current_rel.character_1;
		new_rel.type = current_rel.type;
		new_rel.summary = current_rel.summary;
		all_relationships.push(new_rel);
	}
	return all_relationships;
};

router.get('/characters', function(req, res, next)
{
	Character.find(function(err, characters)
	{
		if(err){return next(err);}
		res.json(characters);
	});
});

router.get('/characters/:character', function(req, res, next)
{
	res.json(req.character);
});

router.get('/relationships/:relationship_single', function(req, res, next)
{
	res.json(req.relationship_single);
});

router.get('/comparison', function(req, res, next)
{
	var name1 = req.query.name1;
	var name2 = req.query.name2;
	var query = Relationship.find().or([{character_1: name1, character_2: name2},{character_1: name2, character_2: name1}]);
	query.exec(function(err, relationships)
	{
		if(err){return next(err);}
		res.json(relationships);
	});
});

router.get('/comparison/characters', function(req,res,next)
{
	var name1 = req.query.char1;
	var name2 = req.query.char2;
	
	var query = Character.find().or([{name: name1}, {name: name2}]);
	query.exec(function(err, characters)
	{
		if(err){return next(err);}
		res.json(characters);
	});

});

router.post('/register', function(req, res, next){
	if(!req.body.username || !req.body.password)
	{
		return res.status(400).json({message: 'Please fill out all fields'});
	}

	var user = new User();
	user.username = req.body.username;
	user.set_password(req.body.password);
	user.save(function(err)
	{
		if(err){return next(err);}
		return res.json({token: user.generate_JWT()});
	});
});

router.post('/login', function(req, res, next)
{
	if(!req.body.username || !req.body.password)
	{
		return res.status(400).json({message: 'Please fill out all fields'});
	}
	passport.authenticate('local', function(err, user, info)
	{
		if(err){return next(err);}
		if(user)
		{
			return res.json({token: user.generate_JWT()});
		}
		else
		{
			return res.status(401).json(info);
		}
	})(req, res, next);
});

module.exports = router;
