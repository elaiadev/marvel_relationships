var mongoose = require('mongoose');

var RelationshipSchema = new mongoose.Schema(
{
	type: String,
	description: String,
	summary: String,
	character_1: String,
	character_2: String
});

mongoose.model('Relationship', RelationshipSchema);