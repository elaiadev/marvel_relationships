var mongoose = require('mongoose');

var CharacterSchema = new mongoose.Schema(
{
	name: String,
	real_name: String,
	description: String,
	slug: String
});

mongoose.model('Character', CharacterSchema);